library my_grid;

import 'package:flutter/material.dart';

/// Les elements seront tous dans des conteneur de meme longueur
class MyGrid extends StatelessWidget {

  /// Nombre maximum d'enfant sur une ligne
  final int axisCount;
  final List<Widget> children;
  MyGrid({ required this.axisCount, required this.children });

  MyGrid.builder({
    required this.axisCount,
    required int count,
    required Widget Function(int) builder
  }): children = List.generate(count, (index) => builder(index));

  //MyGrid.auto({ double minWidth, double maxWidth,  });

  @override
  Widget build(BuildContext context) {
    return Column(children: _build());
  }

  List<Widget> _build() {
    int rowCount = (children.length / axisCount).ceil();
    return List.generate(rowCount, (index) {
      int realIndex = index * axisCount;
      bool lastRow = index == rowCount - 1;
      return Row(children: List.generate(lastRow ? (axisCount - (children.length % axisCount)) : axisCount, (index) => Expanded(child: children[realIndex + index])));
    }, growable: false);
  }

}
